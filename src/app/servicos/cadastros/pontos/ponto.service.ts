import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Ponto } from "../../../models/cadastros/pontos/ponto"
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PontoService {


constructor(private httpClient: HttpClient) { }

obterTodos(): Observable<Ponto[]> {
  return this.httpClient.get<Ponto[]>(`${environment.urlAPI}/Ponto`)
}

obterPorNomeOuId(pesquisa : string): Observable<Ponto[]> {
  if(pesquisa == null || pesquisa == ""){   
    return this.httpClient.get<Ponto[]>(`${environment.urlAPI}/Ponto`);
  }
  else{
    return this.httpClient.get<Ponto[]>(`${environment.urlAPI}/Ponto/ObterPorNomeOuId?pesquisa=${pesquisa}`)
  }
}

obterPorId(id: string): Observable<Ponto[]> {
  return this.httpClient.get<Ponto[]>(`${environment.urlAPI}/Ponto/${id}`)
}

inserir(entidade: Ponto): Observable<Ponto> {
  return this.httpClient.post<Ponto>(`${environment.urlAPI}/Ponto`, entidade );
}

editar(entidade: Ponto): Observable<Ponto> {
  return this.httpClient.put<Ponto>(`${environment.urlAPI}/Ponto`, entidade );
}

excluir(entidade: Ponto): Observable<Ponto> {
  return this.httpClient.delete<Ponto>(`${environment.urlAPI}/Ponto/${entidade.id}`);
}

}
