import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'Pontos', pathMatch: 'full'},
  { path: 'Pontos', loadChildren: () => import('src/app/paginas/cadastros/pontos/pontos.module').then(m => m.PontosModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
