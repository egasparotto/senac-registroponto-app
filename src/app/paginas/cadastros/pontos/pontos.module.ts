import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarComponent } from './listar/listar.component';
import { EditarComponent } from './editar/editar.component';
import { PontosRouting } from './pontos-routing.module.ts';
import { FormatadorDeDatasPipe } from '../../../pipes/Datas/FormatadorDeDatas.pipe';
import { FormatadorDeHorasPipe } from '../../../pipes/Horas/FormatadorDeHoras.pipe';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';

@NgModule({
  imports: [
    CommonModule,
    PontosRouting,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  declarations: [
    ListarComponent,
    FormatadorDeDatasPipe,
    EditarComponent,
    FormatadorDeHorasPipe
  ]
})
export class PontosModule { }
