import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { Ponto } from '../../../../models/cadastros/pontos/ponto';
import { PontoService } from '../../../../servicos/cadastros/pontos/ponto.service';
import { ListarComponent } from '../listar/listar.component';

defineLocale('pt-br', ptBrLocale);

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})


export class EditarComponent implements OnInit { 

  public formulario: FormGroup

  aposModal: Function;

  sender: ListarComponent;

  entidade: Ponto;

  edicao: boolean;

  public operacoes = [
    { nome: 'E', descricao: 'Entrada'},
    { nome: 'S', descricao: 'Saída'}
  ];

  constructor(public bsModalRef: BsModalRef, 
              private fb: FormBuilder,
              private localeService: BsLocaleService) {}
 
  get colaborador() : AbstractControl{
    return this.formulario.get('colaborador');
  }

  get nome() : AbstractControl{
    return this.formulario.get('nome');
  }

  get data() : AbstractControl{
    return this.formulario.get('data');
  }

  get hora() : AbstractControl{
    return this.formulario.get('hora');
  }

  get operacao() : AbstractControl{
    return this.formulario.get('operacao');
  }

  ngOnInit() {
    this.criarForm();
    this.localeService.use('pt-br');
    if(this.entidade){
      this.carregarEntidade(this.entidade);
    }
  }

  criarForm() {
    this.formulario = this.fb.group({
      colaborador: ['', [Validators.required, Validators.pattern('^([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})$')]],
      nome: ['', Validators.required],
      data: ['', Validators.required],
      hora: ['', Validators.required],
      operacao: ['', Validators.required],
      id: ['']
    });
  }

  carregarEntidade(entidade: Ponto){
    var registro = {
      colaborador: entidade.colaborador,
      nome: entidade.nome,
      data: new Date(entidade.dataHoraDoRegistro),
      hora: new Date(entidade.dataHoraDoRegistro),
      operacao: entidade.operacao,
      id: entidade.id
    };
    this.formulario.patchValue(registro);
  }

  enviar() {
    var conteudo = this.formulario.value;
    var data = conteudo.data;
    var id = null;
    if (this.edicao){
      id = conteudo.id;
    }
    data.setHours(conteudo.hora.getHours(), conteudo.hora.getMinutes(), conteudo.hora.getSeconds());
    var registro: Ponto = {
      colaborador: conteudo.colaborador,
      dataHoraDoRegistro: data,
      nome: conteudo.nome,
      operacao: conteudo.operacao,
      id: id
    };
    this.aposModal(registro, this.sender);
    this.bsModalRef.hide();
  }
}
