import { Component, OnInit, TemplateRef } from '@angular/core';
import { Ponto } from '../../../../models/cadastros/pontos/ponto';
import { HorasTrabalhadas } from '../../../../models/cadastros/pontos/horasTrabalhadas';
import { PontoService } from '../../../../servicos/cadastros/pontos/ponto.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EditarComponent } from '../editar/editar.component';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  horasTrabalhadas: HorasTrabalhadas[];
  horasTrabalhadasFiltradas: HorasTrabalhadas[];

  modalRef: BsModalRef;

  _filtroLista: string;

  constructor(private servico:PontoService, private modalService: BsModalService) { }

  get filtroLista(){
    return this._filtroLista;
  }

  set filtroLista(value: string){
    this._filtroLista = value;
    this.filtrarHorasTrbalhadas(this.filtroLista);
  }


  ngOnInit() {
    this.carregarPontos();
  }

  filtrarHorasTrbalhadas(filtroLista: string) {
    this.servico.obterPorNomeOuId(filtroLista).subscribe((data: Ponto[]) => {
      this.horasTrabalhadasFiltradas = this.obterHorasTrabalhadas(data);
    });
  }


   carregarPontos(){
    this.filtroLista = null;
  }

  obterHorasTrabalhadas(registros: Ponto[]): HorasTrabalhadas[]{
    let registroAgrupadosPorColaborador = [];
    let retorno:HorasTrabalhadas[] = [];
    let funcaoCriarHoras = this.criarHorasTrabalhadas;
    registros.forEach(function (a) {
      registroAgrupadosPorColaborador [a.colaborador] = registroAgrupadosPorColaborador [a.colaborador] || [];
      registroAgrupadosPorColaborador [a.colaborador].push(a);
    });

    Object.entries(registroAgrupadosPorColaborador).forEach(function (registros){
      var entrada: Ponto;
      var saida: Ponto;
      var ultimoRegistro : Ponto;
      registros[1].forEach(function (registro : Ponto){
        if(registro.operacao == "E"){
          if (!saida && ultimoRegistro){
            let horasTrabalhadas: HorasTrabalhadas = funcaoCriarHoras(entrada, saida, ultimoRegistro);
            retorno.push(horasTrabalhadas);
          }
          entrada = registro;
          saida = null;
        }
        else if(registro.operacao == "S"){
          saida = registro;          
          let horasTrabalhadas: HorasTrabalhadas = funcaoCriarHoras(entrada, saida, registro);
          retorno.push(horasTrabalhadas);
          entrada = null;
        }      
        ultimoRegistro = registro;
      });
      if(ultimoRegistro && !saida){
        let horasTrabalhadas: HorasTrabalhadas = funcaoCriarHoras(entrada, saida, ultimoRegistro);
        retorno.push(horasTrabalhadas);
        entrada = null;
        saida = null;
      }
    })

    return retorno.sort(this.ordernarHorasTrabalhadas);
  }
 
  ordernarHorasTrabalhadas(obj1 : HorasTrabalhadas, obj2: HorasTrabalhadas){
    var valorComparar1 = obj1.entrada?.dataHoraDoRegistro;
    if(!valorComparar1){
      valorComparar1 = obj1.saida.dataHoraDoRegistro;
    }
    var valorComparar2 = obj2.entrada?.dataHoraDoRegistro;
    if(!valorComparar2){
      valorComparar2 = obj2.saida.dataHoraDoRegistro;
    }
    if (valorComparar1 > valorComparar2) {
        return 1;
    }
    if (valorComparar1 < valorComparar2) {
        return -1;
    }
    return 0;
  }

  criarHorasTrabalhadas(entrada: Ponto, saida: Ponto, registro: Ponto) {
    var horas = null;
    if(entrada && saida){
      horas = (new Date(saida.dataHoraDoRegistro).getTime() - new Date(entrada.dataHoraDoRegistro).getTime()) / 1000;
      horas /= (60 * 60);
    }
    let horasTrabalhadas: HorasTrabalhadas = {
      nome: registro.nome,
      dia: entrada ? new Date(entrada.dataHoraDoRegistro) : new Date(saida.dataHoraDoRegistro),
      horasTrabalhadas: horas,
      entrada: entrada,
      saida: saida
    };
    return horasTrabalhadas;
  }

  abrirModal() {    
    const parametros = {
      aposModal: this.aoInserir,
      sender: this,
      edicao: false
    };
    this.modalRef = this.modalService.show(EditarComponent, {initialState : parametros});
    this.modalRef.setClass('modal-lg');
  }

  aoInserir(entidade : Ponto, sender: ListarComponent){
    sender.servico.inserir(entidade).subscribe(retorno => sender.filtrarHorasTrbalhadas(sender.filtroLista));
  }

  excluir(entidade: Ponto){
    this.servico.excluir(entidade).subscribe(() => {
      this.filtrarHorasTrbalhadas(this.filtroLista);
    });
  }


  editar(entidade: Ponto) {    
    const parametros = {
      aposModal: this.aoEditar,
      sender: this,
      edicao: true,
      entidade: entidade
    };
    this.modalRef = this.modalService.show(EditarComponent, {initialState : parametros});
    this.modalRef.setClass('modal-lg');
  }

  aoEditar(entidade: Ponto, sender: ListarComponent){
    sender.servico.editar(entidade).subscribe(retorno => sender.filtrarHorasTrbalhadas(sender.filtroLista));
  }
}
