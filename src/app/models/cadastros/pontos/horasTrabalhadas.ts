import { Ponto } from './ponto';

export interface HorasTrabalhadas {
    nome: string;
    dia: Date;
    horasTrabalhadas?: number;
    entrada?: Ponto;
    saida?: Ponto;
}
