export interface Ponto {
    id?: string;
    colaborador?: string;
    nome?: string;
    dataHoraDoRegistro?: Date;
    operacao?: string;
}
